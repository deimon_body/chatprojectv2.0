import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter} from 'react-router-dom';
import {createStore,compose,applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import App from "./App.jsx";
import rootReducer from './redux/rootReducer.js';
import reduxThunk from 'redux-thunk';


const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        
      })
    : compose;

const enhancer = composeEnhancers();

const store = createStore(rootReducer,applyMiddleware(reduxThunk))

const app = (
  <Provider store={store}>
      <BrowserRouter>
        <App url="https://edikdolynskyi.github.io/react_sources/messages.json"/>
      </BrowserRouter>
  </Provider>
  
)

ReactDOM.render(
  <React.StrictMode>
    {app}
  </React.StrictMode>,
  document.getElementById("root")
);
