import React from "react";
import {Route,Routes, useNavigate,} from "react-router-dom";
import Chat from './Chat';
import Login from "./components/login/login";
import Users from "./components/users/users";

function App(props){
    return(
    <Routes>
        <Route path="/" element={<Chat url={props.url}/>} />
        <Route path="/login" element={<Login url={props.url}/>} />
        <Route path="/users" element={<Users />} />
    </Routes>
        
    )
}

export default App;