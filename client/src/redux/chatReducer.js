const chatStore = {
    chatData:[],
    isLoaded:false
    // countOfUser:0
}
function getIndexFindElement(id,state){
      return state.findIndex((obj) => {
        return obj.id === id;
      });
}

export default function chatReducer(state=chatStore,action){
    switch(action.type){
        case 'LOAD_NEW_DATA':
            return{
                chatData:[...action.payload.chatData],
                isLoaded:action.payload.isLoaded
            }
        case 'ADD_NEW_MESSAGE':
        
            return{
                ...state,
                chatData:[...state.chatData,action.payload.newMessage]
            }  
        case 'DEL_MESSAGE':
            let del_id = getIndexFindElement (action.payload.messageId,state.chatData)
            const del_currentData = state.chatData;
            del_currentData.splice(del_id,1)
            return{
                 ...state,
                chatData:[...del_currentData]
            } 
        case 'LIKE_MESSAGE':
            let like_id = getIndexFindElement (action.payload.messageId,state.chatData)
            const like_currentData = state.chatData;
            like_currentData[like_id].isLiked = !like_currentData[like_id].isLiked
            return{
                 ...state,
                chatData:[...like_currentData]
            } 
        case 'EDIT_MESSAGE':
            if (action.payload.text.length < 1) return state;
            const edit_id = getIndexFindElement (action.payload.messageId,state.chatData)
            const edit_currentData = state.chatData;
            edit_currentData[edit_id].text = action.payload.text;
            return{
                 ...state,
                chatData:[...edit_currentData]
            } 
        default:
            return state
    }
}