import getTimeHours from "../../helpers/getTimeHours";
export function loadChatData(chatData){
    return{
        type:"LOAD_NEW_DATA",
        payload:{
            chatData:chatData,
            isLoaded:true
        }
    }
}

export function addNewMessage(newMessageObject){
    return {
        type:"ADD_NEW_MESSAGE",
        payload:{
            newMessage:newMessageObject
        }
    }
}

export function deleteMessage(id){
    return {
        type:"DEL_MESSAGE",
        payload:{
            messageId:id
        }
    }
}

export function editMessage(id,newText){
    return {
        type:"EDIT_MESSAGE",
        payload:{
            messageId:id,
            text:newText
        }
    }
}

export function likeCliker(id){
    return {
        type:"LIKE_MESSAGE",
        payload:{
            messageId:id
        }
    }
}


export function asyncLoadData(url,curentUser){
    return async (dispatch)=>{
        const res = await fetch(`${url}`)
        let data = await res.json();

        data = data[0].map((obj) => {
            let isOwnMessage = obj.user == curentUser.user ? true : false ;
            
            return {
              user: obj.user,
              text: obj.text,
              createdAt: getTimeHours(Date.parse(obj.createdAt)),
              avatar: obj.avatar,
              id: obj.id,
              isLiked: false,
              isOwnMessage:isOwnMessage,
              userId:obj.userId
            };
        });
        dispatch(loadChatData(data))
    }
}