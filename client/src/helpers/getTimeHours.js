import dayjs from "dayjs";

function getTimeHours(createdAt) {
  let time = dayjs(createdAt);
  if (String(time.$m).length === 1) {
    time = `${time.$H} : 0${time.$m}`;
  } else {
    time = `${time.$H} : ${time.$m}`;
  }

  return time;
}

export default getTimeHours;
