import { createElement } from "./domHelper";
import "../styles/modal.scss";


const showInputModal = ({ title, onSubmit = () => {}, id = null }) => {
  const rootElement = document.querySelector("#root");

  const modalWrapper = createModalElement(title);
  const modalElement = modalWrapper.querySelector(".modal");

  const submitButton = createElement({
    tagName: "button",
    className: "modal__submit-btn",
    innerElements: ["Submit"],
  });
  const inputElement = createElement({
    tagName: "input",
    className: "modal__input",
    attributes: {
      placeholder: "Put some message...",
    },
  });

  modalElement.append(getFooter([inputElement, submitButton]));
  rootElement.append(modalWrapper);

  submitButton.addEventListener("click", () => {
    modalWrapper.remove();
    if (id) {
      if(inputElement.value.trim().length < 1) return;
      onSubmit(id,inputElement.value)
      return;
    }
    onSubmit();
  });
};

const createModalElement = (title) => {
  const titleElement = createElement({
    tagName: "h1",
    className: "modal__title",
    innerElements: [title],
  });

  const modal = createElement({
    tagName: "div",
    className: "modal",
    innerElements: [titleElement],
  });

  const modalWrapper = createElement({
    tagName: "div",
    className: "modal-wrapper",
    innerElements: [modal],
  });

  return modalWrapper;
};

const getFooter = (children) => {
  return createElement({
    tagName: "div",
    className: "inputs-wrapper",
    innerElements: children,
  });
};

export { showInputModal };
