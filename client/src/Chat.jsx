import { useEffect } from "react";
import "./styles/style.scss";
import Preloader from "./components/Loader/Loader.jsx";
import ChatHeader from "./components/ChatHeader/ChatHeader.jsx";
import { getStorageItem } from "./sessionStorage/sessionHelper";
import MessageList from "./components/MessageList/MessageList.jsx";
import MessageInput from "./components/chatControl/MessageInput.jsx";
import { useNavigate } from "react-router-dom";
import {useDispatch,useSelector} from 'react-redux'
import { asyncLoadData } from "./redux/actions/actions";



function Chat(props){
  const dispatch = useDispatch();
  const isLoaded = useSelector((store)=>store.chatReducer.isLoaded)
  const navigate = useNavigate();

  
  
  useEffect(()=>{
    if(!getStorageItem("currentUser")){
      navigate("/login")
    }else{
      dispatch(asyncLoadData("http://localhost:3050/chatData",getStorageItem("currentUser")))
    }
  },[])

  
  return isLoaded ? (
    
    <section className="chat">
    <ChatHeader />
    <MessageList />
    <MessageInput />
    </section>
  ) : (
    <Preloader />
  );
  
}

export default Chat;
