import "./MessageInput.scss";
import { v4 as uuidv4 } from "uuid";
import { useDispatch } from 'react-redux'
import { useRef } from "react";
import { addNewMessage } from "../../redux/actions/actions";
import { getStorageItem } from "../../sessionStorage/sessionHelper";

function createOwnMessage(text) {
  if (text.value.trim().length < 1) return;
  let currentTime = new Date();

  if (String(currentTime.getMinutes()).length === 1) {
    currentTime = `${currentTime.getHours()}:0${currentTime.getMinutes()}`;
  } else {
    currentTime = `${currentTime.getHours()}:${currentTime.getMinutes()}`;
  }
  const currentUser = getStorageItem("currentUser");
  const userId = currentUser.userId
  const user = currentUser.user
  return {
    text: text.value,
    createdAt: currentTime,
    isOwnMessage: true,
    id: userId,
    user: user,
  };
}

function MessageInput(props) {
  const dispatch = useDispatch()
  const inputRef = useRef(null)
  return (
    <div className="message-input">
      <input 
        ref={inputRef}
        type="text"
        className="message-input-text"
        placeholder="Put your message..."
      />
      <button
        className="message-input-button"

        onClick={() => {
          const messageObj = createOwnMessage(inputRef.current)
          dispatch(addNewMessage(messageObj))
          inputRef.current.value = "";
        }}
      >
        Send Message
      </button>
    </div>
  );
}

export default MessageInput;
