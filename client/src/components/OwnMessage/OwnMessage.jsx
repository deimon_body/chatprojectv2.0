import "./OwnMessage.scss";
import { showInputModal } from "../../helpers/modal";
import {deleteMessage,editMessage} from '../../redux/actions/actions.js'
import { useDispatch } from "react-redux";


function OwnMessage(props) {
  const dispatch = useDispatch();

  return (
    <div className="own-message">
      
        <p className="own-message-text">{props.text}</p>
        <div className="own-message__control-and-time">
          <div className="own-message__controls">
            <div
              className="message-edit own-message__controls--item"
              onClick={() => {
                showInputModal({
                  title: "Edit your message:",
                  onSubmit: (id,newText)=>{
                    dispatch(editMessage(id,newText))
                  },
                  id: props.id,
                });
              }}
            >
              <svg
                aria-hidden="true"
                focusable="false"
                data-prefix="fas"
                data-icon="pen"
                className="svg-inline--fa fa-pen fa-w-16"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"
              >
                <path
                  fill="currentColor"
                  d="M290.74 93.24l128.02 128.02-277.99 277.99-114.14 12.6C11.35 513.54-1.56 500.62.14 485.34l12.7-114.22 277.9-277.88zm207.2-19.06l-60.11-60.11c-18.75-18.75-49.16-18.75-67.91 0l-56.55 56.55 128.02 128.02 56.55-56.55c18.75-18.76 18.75-49.16 0-67.91z"
                ></path>
              </svg>
            </div>
            <div
              className="message-delete own-message__controls--item"
              onClick={() => {
                dispatch(deleteMessage(props.id))
              
              }}
            >
              <svg
                aria-hidden="true"
                focusable="false"
                data-prefix="fas"
                data-icon="trash-alt"
                className="svg-inline--fa fa-trash-alt fa-w-14"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
              >
                <path
                  fill="currentColor"
                  d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"
                ></path>
              </svg>
            </div>
          </div>
          <p className="message-time">{props.time}</p>
        </div>
      </div>
    
  );
}

export default OwnMessage;
