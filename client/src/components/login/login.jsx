import { useNavigate } from "react-router-dom";
import { useRef } from "react";
import { useDispatch } from "react-redux";
import './login.scss';
import { setStorageItem,getStorageItem } from "../../sessionStorage/sessionHelper.js";
import { asyncLoadData } from "../../redux/actions/actions";

function fullFildInput(inpEl){
    if(inpEl.value.length>1){
        inpEl.classList.add("login__input--fullfild");
        return 
    }
    inpEl.classList.remove("login__input--fullfild");
}


async function login(navigate,email,pass,dispatch){
    if(email.length < 1 ){
        alert("Not Valid Email")
        return
    }
    if(pass.length < 1){
        alert("Not Valid Password")
        return
    }
    const data = {
        email:email,
        password:pass
    }
    let res = await fetch("http://localhost:3050/login",{
        method:"POST",
        headers: {'Content-Type': 'application/json'},
        body:JSON.stringify(data)
    });
    let req = await res.json();
    
    if(req.isPassed){
        setStorageItem("currentUser",{user:req.user,isAdmin:req.isAdmin,userId:req.userId}) 
        if(req.isAdmin){
            dispatch(asyncLoadData("http://localhost:3050/chatData",getStorageItem("currentUser")))
            navigate("/users")
            return
        }
        
        navigate('/')
    }
}

function Login(){
    const navigate = useNavigate();
    const email_inp = useRef(null);
    const password_inp = useRef(null);
    const dispatch = useDispatch();
    if(getStorageItem("currentUser")){
        navigate("/");
        return
    }
    return(
        <div className="login">
            <h2 className="login__title">Login</h2>
            <input ref={email_inp} type="text" placeholder="Email..." className="login__input" onChange={(e)=>{
                fullFildInput(e.target)
            }}/>
            <input ref={password_inp} type="text" placeholder="Password..." className="login__input" onChange={(e)=>{
                fullFildInput(e.target)
            }}/>
            <button className="login__btn" onClick={()=>{
                login(navigate,email_inp.current.value,password_inp.current.value,dispatch)
            }}>Submit</button>
        </div>
    )
}

export default Login;