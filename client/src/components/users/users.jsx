import {useDispatch,useSelector} from 'react-redux'
import { Link } from 'react-router-dom';
import UserBlock from './userBlock/userBlock';
import './users.scss';


function Users(){
    const dispatch = useDispatch();
    let chatData = useSelector((store)=>store.chatReducer.chatData)
    
    const uniqData = [];
    console.log(chatData)
    const usersBlock = chatData.map((obj,index)=>{
            return (<UserBlock 
                key={index}
                userName={obj.user}
                userId={obj.userId}
            />)
    })
    return(
        <section className="usersBlock">
        <a href="" className='usersBlock__button'>
           <Link to="/">To the chat</Link>
        </a>
        
        {usersBlock}
        
        </section>
    )
}

export default Users;