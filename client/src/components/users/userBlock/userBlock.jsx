import './userBlock.scss';

function UserBlock(props){
    return(
        <div className="userBlock">
            <h1>{props.userName}</h1>
            <button className="userBlock__button" onClick={()=>{
                console.log(props.userId)
            }}>Delete User</button>
        </div>
    )
}

export default UserBlock;