import "./Message.scss";
import { useDispatch } from 'react-redux';
import { likeCliker } from "../../redux/actions/actions.js";

function Message(props) {
  const dispatch = useDispatch();

  return (
    <div className="message">
      <div className="message-user-avatar">
        <img src={props.imgSrc} alt="author__prop"></img>
      </div>
      <div className="message__textBlock">
        <p className="message-user-name">{props.author}</p>
        <p className="message-text">{props.text}</p>
      </div>
      <div className="message__like-and-time">
        <div className="message-time">
          <p>{props.time}</p>
        </div>
        <div
          className="message-like"
          onClick={() => {
            dispatch(likeCliker(props.id))

          }}
        >
          {props.likeEl}
        </div>
      </div>
    </div>
  );
}

export default Message;
