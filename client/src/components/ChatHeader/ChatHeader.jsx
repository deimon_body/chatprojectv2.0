import { useSelector } from "react-redux";
import "./ChatHeader.scss";

function chatDataFilter(chatInfo) {
  const countOfMessage = chatInfo.length;
  const countOfUsers = new Set(
    chatInfo.map((obj) => {
      return obj.user;
    })
  ).size;

  let lastMessageTime = chatInfo[chatInfo.length - 1].createdAt;
  return { countOfMessage, countOfUsers, lastMessageTime };
}

function ChatHeader() {
  const chatData = useSelector((store)=>store.chatReducer.chatData)
  const chatfilteredInfo = chatDataFilter(chatData);

  return (
    <header className="header">
      <div className="header__chatInfo">
        <h1 className="header-title">My Chat</h1>
        <p className="header-messages-count">Total Messages: {chatfilteredInfo.countOfMessage}</p>
        <p className="header-users-count">Total Users: {chatfilteredInfo.countOfUsers}</p>
      </div>
      <div className="header-last-message-date">
        <p>Last Message at {chatfilteredInfo.lastMessageTime}</p>
      </div>
    </header>
  );
}

export default ChatHeader;
