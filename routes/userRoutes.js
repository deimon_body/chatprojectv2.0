import { Router } from "express";
import UserService from "../services/userService.js";
import { v4 } from "uuid";
const router = Router();
const userService = new UserService("users")


router.get('/',async function(req,res){
    const data = await userService.getAllUsers();
    // res.send(data)
})

router.post('/',function(req,res){
    const {email,password} = req.body
    
    const answer = userService.checkUser(email,password);
    switch(answer.info){
        case "invalidPassword":
            res.send(
                JSON.stringify({
                    isPassed:false,
                    isAdmin:null
                })
            )
            break;
        case "not created":
           const newUser = userService.createUser(email,password,false,"New_User:"+v4(),v4())
           
           res.send(
            JSON.stringify({
                isPassed:true,
                isAdmin:false,
                user:newUser.user,
                userId:newUser.userId
            })
           )
            break;
        case "passed":
            const currentUser = userService.getCurrentUser(email);
            res.send(
                JSON.stringify({
                    isPassed:true,
                    isAdmin:currentUser.isAdmin,
                    user:currentUser.user,
                    userId:currentUser.userId
                })
            )
            break;
        
    }
})
export default router;