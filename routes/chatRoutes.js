import { Router } from "express";
import MessageService from "../services/messageService.js";
// import { getStorageItem } from "../client/src/sessionStorage/sessionHelper.js";
const router = Router();
const messageService = new MessageService("messages")


router.get('/',async function(req,res){
    const data = await messageService.getAllMessage();
    res.send(data)
})


export default router;