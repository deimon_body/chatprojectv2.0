import userRoutes from "./userRoutes.js";
import chatRoutes from './chatRoutes.js';
import usersRoutes from './usersRoutes.js';

export default (app) => {
    app.use('/login', userRoutes);
    app.use("/chatData",chatRoutes);
};