import express from "express";
import cors from "cors";
import "./config/db.js";

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

import routes from "./routes/index.js";
routes(app);

app.use('/', express.static('./client/build'));

const port = 3050;
app.listen(port, () => {
    console.log("The server started at port:http://localhost:3050")
});

export default app;