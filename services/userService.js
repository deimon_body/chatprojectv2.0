import db from "../config/db.js";

class UserService {
    constructor(context){
        this.data = db.data[context]
    }

    getAllUsers(){
        return this.data
    }
    
    getCurrentUser(email){
        return this.data.find((item)=>{
            return item["email"] === email
        })
    }

    checkUser(email,password){
        
        const isEmailValid = this.data.find((item)=>{
            return item["email"] === email
        })
        const isPasswordValid = this.data.find((item)=>{
            return item['email'] === email && item['password'] === password;
        })
        
        if(isEmailValid && !isPasswordValid){
            return {
                info:"invalidPassword"
            }
        }

        if(!isEmailValid) return {info:"not created"} 
        return {
            info:"passed"
        }
    }

    async createUser(email,password,isAdmin,user,id){
        this.data.push({
            "email":email,
            "password":password,
            "isAdmin":isAdmin,
            "user":user,
            "userId":id
        })
        await db.write()
        return  getCurrentUser(email)
    }
}


export default UserService;