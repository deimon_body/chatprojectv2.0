import db from "../config/db.js"
class MessageService{
    constructor(context){
        this.data = db.data[context]
    }

    getAllMessage(){
        return this.data
    }
}

export default MessageService;